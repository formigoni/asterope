# vim: set ts=2 sw=2 tw=0 et :

from conans import ConanFile, CMake, tools


class AsteropeConan(ConanFile):
  name = "asterope"
  version = "0.1"
  license = "BSD 2-clause \"Simplified\" License."
  author = "Ruan E. Formigoni ruanformigoni@gmail.com"
  url = "https://gitlab.com/formigoni/asterope"
  description = "C++ Data Visualization Tool"
  topics = ("cpp" "generic", "visualization","template","algorithm")
  requires = "range-v3/0.10.0@ericniebler/stable", \
    "catch2/2.5.0@bincrafters/stable"
  generators = "cmake"
  exports_sources = "include/*", "cmake/*", "LICENSE", "CMakeLists.txt"
  no_copy_source = True

  def source(self):
    self.run("git clone https://gitlab.com/formigoni/asterope.git")

    # Remove testing
    tools.replace_in_file(
      "CMakeLists.txt",
      "add_subdirectory(test)",
      '',
      strict=True
    )

  def package(self):
    cmake = CMake(self)
    cmake.configure()
    cmake.install()
    self.copy("*.hpp", dst="include", src="include")
    self.copy("LICENSE", ignore_case=True, keep_path=False)

  def package_id(self):
    self.info.header_only()
