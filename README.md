>  vim: set ts=2 sw=2 tw=0 et :
# Asterope - A Field-Coupled Nanocomputing SVG Library

## What is this?

This project is tool for visualizing the graph-level representation 
of a post placement and routing Field-Coupled nanocomputing.
The representation is constructed in the form of a
*scale vector graphics* (SVG) file that uses vectors to draw geometric shapes,
therefore, avoiding image quality losses due to zooming in our out of the
final image.

## Inspiration

| History  | The Pleiades                                                |
| -------  | ------------                                                |
| Asterope is one of seven sisters from the [Greek mythology](https://en.wikipedia.org/wiki/Pleiades_(Greek_mythology)), she was the mother of Oenomaus, son of Ares, the Greek god of War.| <img src="doc/logo/asterope.png"  width="140" height="120">

## Can I see an example? Sure thing!

* I'll show all the functionalities of this library in this document, but
before that, examples can be useful to understand the general idea of what
this tool has to offer.

* The first step is to integrate the library in your project, that is quite trivial using the [cmake](https://cmake.org/) tool for building, testing and packaging software.

1. Let's initialize a new project in an empty folder of your choice, in this folder use the command:
```console
→ git init
```

2. The second step is to add this library as a submodule of your project,
this can be accomplished using the command:
```console
→ git submodule add git@gitlab.com:formigoni/asterope.git lib/asterope
```
The *lib/asterope* part of the command creates a folder for your libraries and 
a subfolder for this one.

3. Now let's create a *CMakeLists.txt* file in the root of your folder as shown below:
```cmake
# This project requires cmake >=3.14.5
cmake_minimum_version(VERSION 3.14.5 FATAL_ERROR)
# Include the external libraries folder
add_subdirectory(lib)
# Add the executable for using the library (We will create main.cpp)
add_executable(main main.cpp)
# Link the library we will create in the next step
target_link_libraries(main PRIVATE asterope)
```

4. Now let's create a *CMakeLists.txt* in the lib folder to integrate
the library in you project.
```cmake
# Create the library
add_library(asterope INTERFACE)
# Include the 'asterope' directory
target_include_directories(asterope INTERFACE "asterope")
```

## TODO - Finish Documenting
