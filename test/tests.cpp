//
// @author      : Ruan E. Formigoni (ruanformigoni@gmail.com)
// @file        : tests
// @created     : Thursday Mar 12, 2020 16:45:04 -03
//

#define CATCH_CONFIG_MAIN

#include <catch2/catch.hpp>
#include "include/asterope/graph/to-dot.cpp"
#include "include/asterope/graph/to-png.cpp"
