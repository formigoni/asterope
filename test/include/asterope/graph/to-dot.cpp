//
// @author      : Ruan E. Formigoni (ruanformigoni@gmail.com)
// @file        : dot-parser
// @created     : Thursday Mar 12, 2020 16:45:55 -03
//

#include <map>
#include <asterope/graph/to-dot.hpp>

namespace asterope::graph::to_dot::test
{

TEST_CASE("dot-parser")
{
  std::multimap<int32_t,int32_t> m
  {
    {0,4},
    {1,4},
    {1,3},
    {1,5},
    {2,3},
    {3,5},
    {4,5}
  };

  auto dot_str {asterope::graph::to_dot::to_dot(m,
    [](auto&& os, auto&& k, auto& v)
      {
        os << k << " -> " << v << std::endl;
      }
    )
  };

  std::string result{
    "Digraph G {\n"
    "0 -> 4\n"
    "1 -> 4\n"
    "1 -> 3\n"
    "1 -> 5\n"
    "2 -> 3\n"
    "3 -> 5\n"
    "4 -> 5\n"
    "}\n"
  };

  REQUIRE(dot_str == result);
}

} // namespace asterope::graph::to_dot::test
