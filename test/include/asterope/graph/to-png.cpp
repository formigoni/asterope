//
// @author      : Ruan E. Formigoni (ruanformigoni@gmail.com)
// @file        : to-png
// @created     : Thursday Mar 12, 2020 19:24:09 -03
//

#pragma once

#include <asterope/graph/to-dot.hpp>
#include <asterope/graph/to-png.hpp>

namespace asterope::graph::to_png::test
{

TEST_CASE("to-png")
{
  std::multimap<int32_t,int32_t> m
  {
    {0,4},
    {1,4},
    {1,3},
    {1,5},
    {2,3},
    {3,5},
    {4,5}
  };

  auto dot_str {asterope::graph::to_dot::to_dot(m,
    [](auto&& os, auto&& k, auto& v)
      {
        os << k << " -> " << v << std::endl;
      }
    )
  };

  asterope::graph::to_png::to_png(dot_str,"my-graph");

  // std::remove("my-graph.png");
}

} // namespace asterope::graph::to_png::test
