//
// @author      : Ruan E. Formigoni (ruanformigoni@gmail.com)
// @file        : dot-parser
// @created     : Thursday Mar 12, 2020 16:35:05 -03
//

#pragma once

#include <range/v3/all.hpp>

namespace asterope::graph::to_dot
{

template<typename Multimap, typename Format>
std::string to_dot(Multimap&& m, Format&& format)
{
  std::ostringstream os;

  os << "Digraph G {" << std::endl;

  ranges::for_each(m, [&os,&format](auto&& e){ format(os,e.first,e.second);  });

  os << "}" << std::endl;

  return os.str();
}

} // namespace asterope::graph::to_dot
