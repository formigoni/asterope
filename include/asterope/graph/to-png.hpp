//
// @author      : Ruan E. Formigoni (ruanformigoni@gmail.com)
// @file        : to-png
// @created     : Thursday Mar 12, 2020 19:11:42 -03
//

#pragma once

#include <fstream>
#include <sstream>

namespace asterope::graph::to_png
{

// TODO check for dot binary before system call
template<typename Str1, typename Str2>
void to_png(Str1&& dot, Str2&& filename)
{
  // Create dotfile
  std::string dotfilename{std::string(filename) + ".dot"};
  std::ofstream dotfile{dotfilename};
  dotfile << dot; dotfile.close();

  // Create command to execute
  std::string outfilename{std::string(filename) + ".png"};
  std::stringstream cmd;
  cmd << "dot -Tpng " << filename << ".dot -o " << outfilename;

  // Perform system call
  system(cmd.str().c_str());

  // Remove dotfile
  std::remove(dotfilename.c_str());
}

} // namespace asterope::graph::to_png
